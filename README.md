﻿# 東京理科大学 生化学2 レポート

## 2016年度

- [嫌気条件下における酵母の解糖反応機構](./glycolysis.md)
- [基質リン酸化と酸化的リン酸化によるATP合成機構](./phosphorylation.md)
- リポたんぱくの種類とそれぞれの性質

## 2017年度

- [2. 解糖反応におけるグリセルアルデヒド三リン酸デヒドロゲナーゼ（GAPDH）の反応と役割](./gapdh.md)
- [5. 電子伝達系における補酵素Q―シトクロムcオキシドレダクターゼの反応機構](./CoQ-Cyt_c_reductase.md)
- [6. 脂肪酸のβ酸化](./fatty_acid_beta_oxidation.md)
- [8. 尿素サイクルの反応機構と生体内での役割](./urea_cycle.md)
- [10.岡崎フラグメント](./okazaki_fragment.md)

## License

Copyright © 2018 yumetodo <yume-wikijp@live.jp>

[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.ja)

自分のレポートに利用するには著作者表示が必要なので、まっとうに適用するとレポート写しがバレるわけですね。
